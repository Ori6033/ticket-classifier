import muiSpacingDefaults from "./muiSpacingDefaults";

describe("muiSpacingDefaults", () => {
  test("default margin in y coordinate is set to 2", () => {
    expect(muiSpacingDefaults.marginY.my).toEqual(2);
  });

  test("default spacing for stacks is set to 4", () => {
    expect(muiSpacingDefaults.stack).toEqual(4);
  });
});
