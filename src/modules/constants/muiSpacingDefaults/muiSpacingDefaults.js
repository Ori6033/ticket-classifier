const muiSpacingDefaults = {
  marginY: {
    my: 2,
  },
  stack: 4,
};

export default muiSpacingDefaults;
