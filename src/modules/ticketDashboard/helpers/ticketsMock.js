const ticketsMock = [
  {
    id: "691ccde3-4838-4b9b-a885-2d09f876586e",
    title: "Blue Eyes White Dragon",
    description: "Lorem Ipsum",
    tags: [
      {
        id: "969c81f0-c726-436f-9113-1d3a709895ac",
        label: "Yugioh",
        color: "primary",
      },
      {
        id: "893dde74-f110-43cd-8d9e-aaa6f8577eb1",
        label: "Kaiba",
        color: "secondary",
      },
    ],
  },
  {
    id: "a4fad348-01df-46c1-b322-6fd8e2894fc8",
    title: "Beetlejuice Beetlejuice Beetlejuice",
    description: "Lorem Ipsum",
    tags: [
      {
        id: "8cf00f63-8b07-4be2-9035-9285eef0d100",
        label: "HEEEEEEY OOOOOH",
        color: "success",
      },
      {
        id: "8dcd55f0-e73e-44aa-b83c-f1761a0ad68e",
        label: "Stack Banana",
        color: "error",
      },
    ],
  },
];

export default ticketsMock;
