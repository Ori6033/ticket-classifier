import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TicketDisplay from "./TicketDisplay";
import ticketsMock from "../helpers/ticketsMock";

const ticketMock = ticketsMock[0];

const createProps = () => {
  return {
    ticket: ticketMock,
    editTicket: jest.fn(),
    removeTicket: jest.fn(),
  };
};

const renderElement = (props) => {
  return render(<TicketDisplay {...props} />);
};

describe("TicketDisplay", () => {
  test("renders with a title", async () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    const title = getByTestId("ticketDisplayTitle").textContent;
    expect(title).toEqual(ticketMock.title);
  });

  test("renders with a description", () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    const description = getByTestId("ticketDisplayDescription").textContent;
    expect(description).toEqual(ticketMock.description);
  });

  test("delete button gets called on click", () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    userEvent.click(getByTestId("ticketDisplayDeleteButton"));
    expect(props.removeTicket).toHaveBeenCalledTimes(1);
  });

  test("edit button gets called on click", () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    userEvent.click(getByTestId("ticketDisplayEditButton"));
    expect(props.editTicket).toHaveBeenCalledTimes(1);
  });
});
