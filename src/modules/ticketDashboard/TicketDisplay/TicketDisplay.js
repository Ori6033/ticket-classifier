import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import TagList from "modules/common/TagList/TagList";
import ActionButtonGroup from "modules/common/ActionButtonGroup/ActionButtonGroup";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function TicketDisplay({ ticket, removeTicket, editTicket }) {
  return (
    <Stack
      direction="column"
      justifyContent="space-between"
      spacing={muiSpacingDefaults.stack}
      sx={muiSpacingDefaults.marginY}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={8}>
          <Typography
            color="textPrimary"
            variant="h2"
            data-testid="ticketDisplayTitle"
          >
            {ticket.title}
          </Typography>
        </Grid>
        <ActionButtonGroup justifyContent="flex-end">
          <Button
            color="error"
            variant="outlined"
            startIcon={<DeleteIcon />}
            onClick={() => removeTicket(ticket.id)}
            data-testid="ticketDisplayDeleteButton"
          >
            Delete
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<EditIcon />}
            onClick={() => editTicket(ticket)}
            data-testid="ticketDisplayEditButton"
          >
            Edit
          </Button>
        </ActionButtonGroup>
      </Grid>
      <Typography color="textPrimary" data-testid="ticketDisplayDescription">
        {ticket.description}
      </Typography>
      <TagList tags={ticket.tags} />
    </Stack>
  );
}

export default TicketDisplay;
