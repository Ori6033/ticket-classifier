import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import ActionButtonGroup from "modules/common/ActionButtonGroup/ActionButtonGroup";
import TagList from "modules/common/TagList/TagList";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function TicketForm({ ticket, updateTicket, cancelTicket }) {
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({ mode: "onChange" });
  const ticketBeforeChanges = ticket;
  const defaultTags = [
    ...ticket.tags,
    {
      id: "67426261-ae38-45e3-9608-22e816c667e6",
      label: "Dyson Sphere",
      color: "primary",
    },
    {
      id: "67226261-ae38-45e3-9608-22e816c66312e",
      label: "Mars Base",
      color: "error",
    },
  ];

  useEffect(() => reset(ticketBeforeChanges), [reset, ticketBeforeChanges]);

  return (
    <Stack
      component="form"
      direction="column"
      justifyContent="space-between"
      spacing={muiSpacingDefaults.stack}
      sx={muiSpacingDefaults.marginY}
      onSubmit={handleSubmit(updateTicket)}
      data-testid="ticketForm"
    >
      <Controller
        name="title"
        control={control}
        defaultValue={ticket.title || ""}
        rules={{ required: true }}
        render={({ field }) => (
          <TextField
            required
            variant="standard"
            label="Title"
            error={errors.title}
            helperText={errors.title ? "Title is required" : ""}
            data-testid="ticketFormTitle"
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        defaultValue={ticket.description || ""}
        rules={{ required: true }}
        render={({ field }) => (
          <TextField
            multiline
            required
            variant="standard"
            label="Description"
            error={errors.description}
            helperText={errors.description ? "Description is required" : ""}
            data-testid="ticketFormDescription"
            {...field}
          />
        )}
      />
      <Controller
        name="tags"
        control={control}
        defaultValue={ticket.tags || []}
        render={({ field }) => (
          <>
            <InputLabel id="tags-select-label" shrink={true}>
              Tags
            </InputLabel>
            <Select
              multiple
              variant="standard"
              labelId="tags-select-label"
              renderValue={(selectedTags) => <TagList tags={selectedTags} />}
              data-testid="ticketFormSelect"
              {...field}
            >
              {defaultTags.map((tag) => (
                <MenuItem
                  key={tag.id}
                  value={tag}
                  data-testid="ticketFormMenuItem"
                >
                  {tag.label}
                </MenuItem>
              ))}
            </Select>
          </>
        )}
      />
      <ActionButtonGroup justifyContent="flex-end">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => cancelTicket(ticket)}
          data-testid="ticketFormCancelButton"
        >
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          data-testid="ticketFormSaveTicketButton"
        >
          Save Ticket
        </Button>
      </ActionButtonGroup>
    </Stack>
  );
}

export default TicketForm;
