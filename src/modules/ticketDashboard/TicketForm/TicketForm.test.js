import { render, fireEvent, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TicketForm from "./TicketForm";
import ticketsMock from "../helpers/ticketsMock";

const ticketMock = ticketsMock[0];

const createProps = () => {
  return {
    ticket: ticketMock,
    updateTicket: jest.fn(),
    cancelTicket: jest.fn(),
  };
};

const renderElement = (props) => {
  return render(<TicketForm {...props} />);
};

describe("TicketForm", () => {
  test("renders with a title", async () => {
    const props = createProps();
    const { getByDisplayValue } = renderElement(props);
    const title = getByDisplayValue(ticketMock.title);

    expect(title).toBeInTheDocument();
  });

  test("renders with a description", () => {
    const props = createProps();
    const { getByDisplayValue } = renderElement(props);
    const description = getByDisplayValue(ticketMock.description);

    expect(description).toBeInTheDocument();
  });

  test("renders select input for tags", () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    const element = getByTestId("ticketFormSelect");

    expect(element).toBeInTheDocument();
  });

  test("cancel button gets called on click", () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    userEvent.click(getByTestId("ticketFormCancelButton"));

    expect(props.cancelTicket).toHaveBeenCalledTimes(1);
  });

  test("ticket does not update if form is submitted with no changes", async () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    userEvent.click(getByTestId("ticketFormSaveTicketButton"));

    await waitFor(() => {
      expect(props.updateTicket).toHaveBeenCalledTimes(0);
    });
  });

  test("ticket is submitted when the form title is updated", async () => {
    const props = createProps();
    const { getByDisplayValue, getByTestId } = renderElement(props);
    const value = "boop";

    const input = getByDisplayValue(ticketMock.title);
    fireEvent.change(input, { target: { value } });
    userEvent.click(getByTestId("ticketFormSaveTicketButton"));

    await waitFor(() => {
      expect(input.value).toEqual(value);
    });
    expect(props.updateTicket).toHaveBeenCalledTimes(1);
  });

  test("ticket is submitted when the form description is updated", async () => {
    const props = createProps();
    const { getByDisplayValue, getByTestId } = renderElement(props);
    const value = "boop";

    const input = getByDisplayValue(ticketMock.description);
    fireEvent.change(input, { target: { value } });
    userEvent.click(getByTestId("ticketFormSaveTicketButton"));

    await waitFor(() => {
      expect(input.value).toEqual(value);
    });
    expect(props.updateTicket).toHaveBeenCalledTimes(1);
  });

  // TODO: This test is passing, but is a false positive
  test.skip("ticket is submitted when the tag form is updated", async () => {
    const props = createProps();
    const { getByTestId } = renderElement(props);
    const input = getByTestId("ticketFormSelect");
    fireEvent.keyDown(input, { key: "Enter" });
    fireEvent.keyDown(input, { key: "Enter" });
    userEvent.click(getByTestId("ticketFormSaveTicketButton"));

    await waitFor(() => {
      expect(props.updateTicket).toHaveBeenCalledTimes(1);
    });
  });
});
