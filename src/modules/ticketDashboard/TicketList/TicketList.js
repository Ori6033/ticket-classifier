import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import TicketDisplay from "modules/ticketDashboard/TicketDisplay/TicketDisplay";
import TicketForm from "modules/ticketDashboard/TicketForm/TicketForm";
import ticketsMock from "../helpers/ticketsMock";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function TicketList() {
  const [tickets, setTickets] = useState([]);

  useEffect(() => {
    setTickets(createTicketsMetadata(ticketsMock));

    return () => setTickets(() => []);
  }, []);

  const createTicketsMetadata = (ticketsToUpdate) => {
    return ticketsToUpdate.map((ticket) => {
      ticket.metadata = { displayTicketForm: false };
      return ticket;
    });
  };

  const removeTicket = (ticketID) => {
    setTickets(tickets.filter((ticket) => ticket.id !== ticketID));
  };

  const updateTicket = (updatedTicket) => {
    setTickets((prevTickets) => {
      return prevTickets.map((ticket) =>
        ticket.id === updatedTicket.id ? updatedTicket : ticket
      );
    });
  };

  const toggleDisplayTicketForm = (ticketToUpdate) => {
    const clonedTicket = JSON.parse(JSON.stringify(ticketToUpdate));
    clonedTicket.metadata.displayTicketForm =
      !clonedTicket.metadata.displayTicketForm;
    updateTicket(clonedTicket);
  };

  return (
    <Box sx={muiSpacingDefaults.marginY} data-testid="ticketList">
      {tickets.map((ticket) => (
        <Card
          key={ticket.id}
          sx={muiSpacingDefaults.marginY}
          data-testid="ticketCard"
        >
          <CardContent spacing={4}>
            {ticket.metadata.displayTicketForm ? (
              <TicketForm
                ticket={ticket}
                updateTicket={updateTicket}
                cancelTicket={toggleDisplayTicketForm}
              />
            ) : (
              <TicketDisplay
                ticket={ticket}
                removeTicket={removeTicket}
                editTicket={toggleDisplayTicketForm}
              />
            )}
          </CardContent>
        </Card>
      ))}
      <Stack direction="row" justifyContent="flex-end" alignItems="flex-end">
        <Fab color="primary" aria-label="add">
          <AddIcon />
        </Fab>
      </Stack>
    </Box>
  );
}

export default TicketList;
