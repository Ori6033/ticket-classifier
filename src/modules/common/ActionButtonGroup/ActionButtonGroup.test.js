import { render } from "@testing-library/react";
import ActionButtonGroup from "./ActionButtonGroup";

const renderElement = () =>
  render(
    <ActionButtonGroup>
      <span data-testid="child">child</span>
    </ActionButtonGroup>
  );

describe("ActionButtonGroups", () => {
  test("renders component", () => {
    const { getByTestId } = renderElement();
    expect(getByTestId("actionButtonGroup")).toBeInTheDocument();
  });

  test("renders child", () => {
    const { getByTestId } = renderElement();
    expect(getByTestId("child")).toBeInTheDocument();
  });
});
