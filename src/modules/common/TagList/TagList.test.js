import { render } from "@testing-library/react";
import TagList from "./TagList";

const tagsMock = [
  {
    id: "67926261-ae38-45e3-9608-22e816c667e6",
    label: "Dyson Sphere",
    color: "primary",
  },
  {
    id: "67926261-ae38-45e3-9608-22e816c66312e",
    label: "Mars Base",
    color: "error",
  },
];

const renderComponent = () => render(<TagList tags={tagsMock} />);

describe("TagList", () => {
  test("renders component", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("tagList");
    expect(element).toBeInTheDocument();
  });

  test("tagListChip renders tags passed as props", async () => {
    const { getAllByTestId } = renderComponent();
    const elements = getAllByTestId("tagListChip");
    expect(elements.length).toBe(tagsMock.length);
  });

  test("tagListChip are the same as the tags passed as props", async () => {
    const { getAllByTestId } = renderComponent();
    const elements = getAllByTestId("tagListChip");
  });
});
